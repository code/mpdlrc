mpdlrc
======

Print timed lyrics from an LRC file for MPD's currently playing song
line-by-line to `stdout`: <https://en.wikipedia.org/wiki/LRC_%28file_format%29>

Needs `Net::MPD` and a few core Perl packages. Expects to find lyrics named
`Artist - Title.lrc` in `~/.lyrics`. An LRC file I made for Darkthrone's "To
Walk The Infernal Fields" from their 1993 album "Under a Funeral Moon" is
included for testing.

This could maybe be used with `libnotify` or a similar desktop notification
tool to display lyrics in a corner as you work on other things. I've included a
simple Bash wrapper around `notify-send(1)` which works for me.

You can install both with `make install`. The default `PREFIX` is `/usr/local`.
You may prefer `$HOME/.local`, like I do.

Legacy
------

This code is not maintained. There are plans to repackage it as a Perl 5 module
unto itself, using [Music::Lyrics::LRC][1]. The latter module is
feature-complete and available on CPAN, but a version of `mpdlrc` that actually
uses it has yet to be written.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [Artistic License 2.0][3].

[1]: https://sanctum.geek.nz/cgit/Music-Lyrics-LRC.git/about/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/artistic-license-2.0
